# Postboard

Welcome to Postboard. 

- View a paginated view of posts with basic information on who posts were from and links to the user's page 

- View a paginated view of users with basic information and links to the user's page

- Posts detail pages with all the most content plus all the comments and link to who posted it.

- User detail page with all the user information and a paginated view of all their posts


# Requirements for running 
Node 9.2.0 or higher  
Chrome Browser

# Notes
This is a very minimal application. However, it's responsive, uses css animations, displays a loader when fetching data and has a nodejs server.
Given a bit more time I would have:

- added more design.

- used sass to modularise the styles 

- cache api responses on the backend to handle server load

- cache headers for static files

- handled pagination on the backend to reduce memory usage in the UI

- find more ways to make similar parts of the UI reusable


# Technologies used
React  
Nextjs  
nodejs  
express  
graphql  
css/css3  