const axios = require('axios');

const http = axios.create({
    baseURL: '/graphql'
});


function createQuery(query) {
    return (variables) => {
        return http({
            method: 'post',
            data: {
                query,
                variables
            }
        }).then((result) => {
            return result.data;
        });
    }
}

export const postListQuery = createQuery(`
{
    postList {
        posts {
            id
            title
            user {
                id
                username
            }
        }
        total
    }      
}
`);

export const userListQuery = createQuery(`
{
    userList {
        users {
              id
              username
                company {
                    name
                }
        }
    
    total
    }
  
}
`);

export const userQuery = createQuery(`
query GetUser($id: Int!) {

  user(id: $id) {
  	id
    name
    username
    email
    phone
    website
    company {
      name
      catchPhrase
      bs
    }
    address {
      street
      suite
      city
      zipcode
      geo {
        lat
        lng
      }
    }
    posts {
      id
      title
    }
  }

}
`);

export const postQuery = createQuery(`
query GetPost($id: Int!){
  
  post(id: $id) {
    id
    title
    body
    user {
      id
      username
    }
    comments {
      id
      name
      email
      body
    }
  }
}
`);
