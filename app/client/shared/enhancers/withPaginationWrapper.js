import React from "react";
import Pagination from "../../components/Pagination";

export default (title, limit = 12) => function withPaginationWrapper(WrappedComponent) {
    class WrapperComponent extends React.Component {

        state = {
            data: [],
            total: 0,
            sliceFrom: 0,
            startFrom: 0
        };

        render() {
            const {data, sliceFrom, startFrom, total} = this.state;
            const {hideBottom, className = ''} = this.props;
            const isAtLimit = sliceFrom + limit >= total;
            return (
                <div className={className}>
                    <h1>{title}</h1>
                    <div className="mdl-grid">
                        <Pagination
                            total={this.getPaginationTotal()}
                            onNext={this.handlePaginationNext(false)}
                            onPrevious={this.handlePaginationPrevious(false)}
                            startFrom={startFrom}
                            isAtLimit={isAtLimit}
                        />
                        <div className="mdl-grid">
                            <WrappedComponent
                                data={data}
                                sliceFrom={sliceFrom}
                                sliceTo={limit + sliceFrom}
                                {...this.props}/>
                        </div>
                        {!hideBottom && <Pagination
                            justify={Pagination.Justify.Right}
                            total={this.getPaginationTotal()}
                            onNext={this.handlePaginationNext(true)}
                            onPrevious={this.handlePaginationPrevious(true)}
                            startFrom={startFrom}
                            isAtLimit={isAtLimit}
                        />}
                    </div>
                </div>
            )
        }

        getPaginationTotal() {
            return Math.round(this.state.total / limit)
        }

        handlePaginationNext = isBottom => (startFrom) => {
            this.scrollToTop(isBottom);
            const sliceFrom = this.state.sliceFrom + limit;
            this.setState({sliceFrom, startFrom})
        };

        handlePaginationPrevious = isBottom => (startFrom) => {
            this.scrollToTop(isBottom);
            const sliceFrom = this.state.sliceFrom - limit;
            this.setState({sliceFrom, startFrom})
        };

        componentDidMount() {
            const {data} = this.props;
            if (data) {
                const rootKey = Object.keys(data)[0];
                this.setState({data, total: data[rootKey].total})
            }
        }

        scrollToTop(shouldScroll) {
            shouldScroll && window.scrollTo(0, 0);
        }
    }

    return WrapperComponent;
}
