import React from "react";
import Loader from "../../components/Loader";


export default (query, getParams = props => props) => function withQuery(WrappedComponent) {
    class WrapperComponent extends React.Component {
        state = {
            data: null
        };

        render() {
            return !this.state.data
                ? <Loader/>
                : <WrappedComponent data={this.state.data} {...this.props}/>
        }

        componentDidMount() {
            query(getParams(this.props)).then(data => this.setState({data: data.data}));
        }
    }

    return WrapperComponent;
}