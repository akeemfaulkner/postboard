import React, {Fragment} from "react";
import Head from "../../components/Head";

export default function withPageWrapper(WrappedComponent) {
    class WrapperComponent extends React.Component {

        render() {
            return (
                <Fragment>
                    <Head/>
                    <div className="mdl-layout mdl-layout--fixed-header">
                        <header className="mdl-layout__header mdl-layout__header--scroll">
                            <div className="mdl-layout__header-row">
                                <a href="/" className="logo-link"><span className="mdl-layout-title">Postboard</span></a>
                                <div className="mdl-layout-spacer"/>
                                <nav className="mdl-navigation">
                                    <a className={`mdl-navigation__link ${this.getActiveLinkClass('/')}`} href="/" >Posts</a>
                                    <a className={`mdl-navigation__link ${this.getActiveLinkClass('/users')}`} href="/users">Users</a>
                                </nav>
                            </div>
                        </header>

                        <main className="mdl-layout__content">
                            <div className="mdl-grid">
                                <WrappedComponent {...this.props}/>
                            </div>
                        </main>
                        <div id="modal-root"/>
                    </div>
                </Fragment>
            )
        }

        getActiveLinkClass(path) {
            return this.props.url.asPath === path
                ? 'mdl-navigation__link--is-active'
                : ''
        }
    }

    return WrapperComponent;
}