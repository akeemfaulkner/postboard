import React from "react";
import Button from "./Button";

export default function IconButton({icon, ...rest}) {
    return (
        <Button {...rest}>
            <i className="material-icons">{icon}</i>
        </Button>
    )
}