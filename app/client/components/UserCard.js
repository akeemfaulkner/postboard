import React from "react";
import Card from "./Card";

export default function UserCard({id, username, company, delay}) {
    return (
        <Card delay={delay} title={username} id={id}>
            <div className="mdl-card__supporting-text">
                company: <b>{company.name}</b>
            </div>
            <div className="mdl-card__actions mdl-card--border">
                <a className="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href={`/users/${id}`}>
                    View Profile
                </a>
            </div>
        </Card>
    )
}