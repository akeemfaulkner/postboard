import React from "react";

export default function Detail({title, detail, Component = 'div', ...rest}) {
    return (
        <Component {...rest}>
            <b>{title}:</b> {detail}
        </Component>
    )
}