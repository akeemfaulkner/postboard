import React from "react";
import * as ReactDOM from "react-dom";
import {DoubleBounce} from 'better-react-spinkit'


export default class Loader extends React.Component {
    constructor(props) {
        super(props);
        if (typeof document !== 'undefined') {
            this.el = document.createElement('div');
        }
    }

    componentDidMount() {

        this.root = document.getElementById('modal-root');
        this.root.appendChild(this.el);
    }

    componentWillUnmount() {
        this.root.removeChild(this.el);
    }

    render() {
        if (typeof document !== 'undefined') {
            return ReactDOM.createPortal(
                <div className="loader"><DoubleBounce size={100}/></div>,
                this.el,
            );
        }
        return null;
    }
}