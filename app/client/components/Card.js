import React from "react";

export default function Card({title, delay = 0, id, children}) {
    return (
        <div className="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--3-col"
             style={{animationDelay: getDelay(delay)}}>
            <div className="mdl-card__title mdl-card--expand"
                 style={{background: `url(https://picsum.photos/200/300?image=${id}) bottom right 15% no-repeat #46B6AC`}}>
                <h2 className="mdl-card__title-text">{title}</h2>
            </div>

            {children}
        </div>
    )
}

function getDelay(delay) {
    return delay < 10
        ? `.${delay}s`
        : `${delay / 10}s`
}