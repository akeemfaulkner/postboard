import React from "react";
import Card from "./Card";

export default function PostCard({id, title, user, delay}) {
    return (
        <Card delay={delay} id={id} title={title}>

            <div className="mdl-card__supporting-text">
                By:  <a  href={`/users/${user.id}`}><b>{user.username}</b></a>
            </div>
            <div className="mdl-card__actions mdl-card--border">
                <a className="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href={`/posts/${id}`}>
                    Read More
                </a>
            </div>
        </Card>
    )
}