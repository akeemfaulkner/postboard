import React from "react";

export default function Button({children, ...rest}) {
    return (
        <button className="mdl-button" {...rest}>
            {children}
        </button>
    )
}