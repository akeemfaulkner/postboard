import React from 'react';
import IconButton from "./IconButton";

export default class Pagination extends React.Component {

    static Justify = {
        Left: 'pagination--justify-left',
        Right: 'pagination--justify-right',
    };

    static defaultProps = {
        total: 10,
        justify: Pagination.Justify.Left
    };

    state = {
        startFrom: 0
    };

    render() {
        const {total, justify, isAtLimit} = this.props;
        return (
            <div className={`mdl-grid pagination ${justify}`}>
                <IconButton
                    icon="keyboard_arrow_left"
                    onClick={this.previous}
                    disabled={this.shouldDisableButton(0)}/>

                <IconButton
                    icon="keyboard_arrow_right"
                    onClick={this.next}
                    disabled={this.shouldDisableButton(total) || isAtLimit}/>
            </div>
        )
    }

    componentDidUpdate(){
        const startFrom = this.props.startFrom;
        if(typeof startFrom !== 'undefined' && startFrom !== this.state.startFrom) {
            this.setState({startFrom})
        }
    }

    shouldDisableButton(index) {
        return this.state.startFrom === index || this.props.total === 1;
    }

    previous = () => {
        const startFrom = this.state.startFrom;
        if(startFrom > -1) {
            const nextStartFrom = startFrom - 1;
            this.setState({startFrom: nextStartFrom}, ()=>{
                this.props.onPrevious(nextStartFrom);
            });
        }
    };

    next = () => {

        const startFrom = this.state.startFrom;
        if (startFrom !== this.props.total) {
            const nextStartFrom = startFrom + 1;
            this.setState({startFrom: nextStartFrom}, () => {
                this.props.onNext(nextStartFrom);
            });
        }

    };

}
