import React from "react";
import withPageWrapper from "../shared/enhancers/withPageWrapper";
import PostCard from "../components/PostCard";
import withQuery from "../shared/enhancers/withQuery";
import {postListQuery} from "../shared/query";
import withPaginationWrapper from "../shared/enhancers/withPaginationWrapper";
import {compose} from "../shared/utils";

class Index extends React.Component {

    render() {
        const {data: {postList: {posts}}, sliceFrom, sliceTo} = this.props;
        return posts.slice(sliceFrom, sliceTo).map((post, index) => {
            return <PostCard key={post.id} delay={index} {...post}/>
        })
    }

}

export default compose(
    withPageWrapper,
    withQuery(postListQuery),
    withPaginationWrapper('Posts', 12)
)(Index);
