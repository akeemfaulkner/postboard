import React from "react";
import withPageWrapper from "../shared/enhancers/withPageWrapper";
import withQuery from "../shared/enhancers/withQuery";
import {postQuery} from "../shared/query";
import {compose} from "../shared/utils";
import Detail from "../components/Detail";

class Post extends React.Component {

    render() {
        const {data: {post: {comments, user}, post}} = this.props;

        return (
            <div className="mdl-shadow--2dp mdl-cell--12-col">
                <div className="mdl-card__title">
                    <h2 className="mdl-card__title-text">
                        <small>Post</small>
                        &nbsp;{post.title}
                    </h2>
                </div>
                <hr/>
                <div className="mdl-card__supporting-text">
                    <Detail
                        Component="a"
                        title={"By"}
                        detail={user.username}
                        href={`/users/${user.id}`}
                    />
                    <h3>{post.body}</h3>
                </div>
                <div className="mdl-card__supporting-text">

                    <h5>Comments</h5>
                    <div className="mdl-grid">
                        {
                            comments.map(comment => (
                                <div className="mdl-card comment-card mdl-cell mdl-cell--8-col" key={comment.id}>
                                    <div className="comment-card__name">
                                        <h4>{comment.name}</h4>
                                    </div>
                                    <div className="mdl-color-text--grey-600 mdl-card__supporting-text">
                                        {comment.body}
                                    </div>
                                    <div className="mdl-card__supporting-text meta mdl-color-text--grey-600">
                                        <small>From:</small>
                                        &nbsp;<strong>{comment.email}</strong>
                                    </div>
                                </div>
                            ))
                        }

                    </div>
                </div>
            </div>
        );
    }

}


export default compose(
    withPageWrapper,
    withQuery(postQuery, ({url}) => {
        return {id: url.query.id}
    })
)(Post)
