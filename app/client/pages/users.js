import React from "react";
import withPageWrapper from "../shared/enhancers/withPageWrapper";
import withQuery from "../shared/enhancers/withQuery";
import {userListQuery} from "../shared/query";
import UserCard from "../components/UserCard";
import withPaginationWrapper from "../shared/enhancers/withPaginationWrapper";
import {compose} from "../shared/utils";

class Users extends React.Component {

    render() {
        const {data: {userList: {users}}, sliceFrom, sliceTo} = this.props;
        return users.slice(sliceFrom, sliceTo).map((user, index) => {
            return <UserCard key={user.id} delay={index} {...user}/>
        })
    }


}


export default compose(
    withPageWrapper,
    withQuery(userListQuery),
    withPaginationWrapper('Users', 12)
)(Users);
