import React from "react";
import withPageWrapper from "../shared/enhancers/withPageWrapper";
import withQuery from "../shared/enhancers/withQuery";
import {userQuery} from "../shared/query";
import {compose} from "../shared/utils";
import PostCard from "../components/PostCard";
import Detail from "../components/Detail";
import withPaginationWrapper from "../shared/enhancers/withPaginationWrapper";

function UserPosts({data: {postList: {posts}}, user, sliceFrom, sliceTo}) {
    return posts.slice(sliceFrom, sliceTo).map((post, index) => {
        return <PostCard key={post.id} delay={index} {...post} user={user}/>
    })
}

UserPosts = withPaginationWrapper('Posts', 4)(UserPosts);

class User extends React.Component {

    render() {

        const {data: {user, user: {address, company, posts}}} = this.props;
        return (
            <div className="mdl-shadow--2dp mdl-cell--12-col">
                <div className="mdl-card__title">
                    <h2 className="mdl-card__title-text">
                        <small>User</small>
                        &nbsp;{user.username}</h2>
                </div>
                <div className="mdl-card__supporting-text">
                    <Detail
                        title={"Name"}
                        detail={user.name}
                    />
                    <Detail
                        title={"Phone"}
                        detail={user.phone}
                    />
                    <Detail
                        title={"Website"}
                        detail={user.website}
                    />
                    <Detail
                        title={"Email"}
                        detail={user.email}
                    />
                    <hr/>
                    <h5>Address</h5>
                    <Detail
                        title={"Suite"}
                        detail={address.suite}
                    />
                    <Detail
                        title={"Street"}
                        detail={address.street}
                    />
                    <Detail
                        title={"City"}
                        detail={address.city}
                    />
                    <Detail
                        title={"Zip Code"}
                        detail={address.zipcode}
                    />
                    <br/>
                    <iframe
                        src={`http://maps.google.com/maps?q=${address.geo.lat}, ${address.geo.lng}&z=15&output=embed`}
                        height="270"
                        width="300"
                        frameBorder="0" style={{border: 0}}/>
                    <hr/>
                    <h5>Place of Work: {company.name}</h5>
                    <p><b>{company.catchPhrase}</b> <em>{company.bs}</em></p>
                    <hr/>
                    <UserPosts
                        className="single-user__posts"
                        data={{postList: {posts, total: posts.length}}}
                        user={user}
                        hideBottom={true}
                    />
                </div>

            </div>
        )
    }

}


export default compose(
    withPageWrapper,
    withQuery(userQuery, ({url}) => {
        return {id: url.query.id}
    })
)(User)
