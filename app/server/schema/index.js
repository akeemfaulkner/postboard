const http = require('../shared/http');
const graphql = require('graphql');
const {GraphQLSchema, GraphQLList} = graphql;

const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt
} = graphql;

const GeoType = new GraphQLObjectType({
    name: 'GeoType',
    fields: {
        lat: {type: GraphQLString},
        lng: {type: GraphQLString},
    }
});

const AddressType = new GraphQLObjectType({
    name: 'AddressType',
    fields: {
        street: {type: GraphQLString},
        suite: {type: GraphQLString},
        city: {type: GraphQLString},
        zipcode: {type: GraphQLString},
        geo: {type: GeoType}
    }
});

const CompanyType = new GraphQLObjectType({
    name: 'CompanyType',
    fields: {
        name: {type: GraphQLString},
        catchPhrase: {type: GraphQLString},
        bs: {type: GraphQLString},
    }
});

const CommentType = new GraphQLObjectType({
    name: 'CommentType',
    fields: () => ({
        id: {type: GraphQLInt},
        name: {type: GraphQLString},
        email: {type: GraphQLString},
        body: {type: GraphQLString},
    })
});

const PostType = new GraphQLObjectType({
    name: 'PostType',
    fields: () => ({
        id: {type: GraphQLInt},
        title: {type: GraphQLString},
        body: {type: GraphQLString},
        user: {
            type: UserType,
            resolve(parentValue) {
                return getUsers().then(users => getUserById(users, parentValue.userId))
            }
        },
        comments: {
            type: new GraphQLList(CommentType),
            resolve(parentValue) {
                return getComments().then(comments => getCommentsPostId(comments, parentValue.id))
            }
        },
    })
});


const UserType = new GraphQLObjectType({
    name: 'UserType',
    fields: {
        id: {type: GraphQLInt},
        name: {type: GraphQLString},
        username: {type: GraphQLString},
        phone: {type: GraphQLString},
        website: {type: GraphQLString},
        email: {type: GraphQLString},
        address: {type: AddressType},
        company: {type: CompanyType},
        posts: {
            type: new GraphQLList(PostType),
            resolve(parentValue) {
                return getPosts().then(posts => posts.filter(post => post.userId === parentValue.id))
            }

        },
    }
});

const UserListType = new GraphQLObjectType({
    name: 'UserListType',
    fields: {
        users: {type: new GraphQLList(UserType)},
        total: {type: GraphQLInt},
    }
});
const PostListType = new GraphQLObjectType({
    name: 'PostListType',
    fields: {
        posts: {type: new GraphQLList(PostType)},
        total: {type: GraphQLInt},
    }
});


const RootQueryType = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        postList: {
            type: PostListType,
            resolve: async () => {
                const posts = await getPosts();
                return {
                    posts,
                    total: posts.length
                }
            }
        },
        userList: {
            type: UserListType,
            resolve: async () => {
                const users = await getUsers();
                return {
                    users,
                    total: users.length
                }
            }
        },
        post: {
            type: PostType,
            args: {
                id: {type: GraphQLInt}
            },
            resolve(parentValue, {id}) {
                return getPosts().then(posts => posts.find(post => post.id === id))
            }
        },
        user: {
            type: UserType,
            args: {
                id: {type: GraphQLInt}
            },
            resolve(parentValue, {id}) {
                return getUsers().then(users => getUserById(users, id))
            }
        },

    }
});

module.exports = new GraphQLSchema({
    query: RootQueryType
});

function getPosts() {
    return http.get('/posts').then(response => {
        return response.data;
    });
}

function getUsers() {
    return http.get('/users').then(response => {
        return response.data;
    });
}

function getUserById(users, id) {
    return users.find(user => user.id === id);
}

function getComments() {
    return http.get('/comments').then(response => {
        return response.data;
    });
}

function getCommentsPostId(comments, id) {
    return comments.filter(comment => comment.postId === id);
}