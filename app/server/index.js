const express = require('express');
const next = require('next');
const path = require('path');
const expressGraphQL = require('express-graphql');
const dev = process.env.NODE_ENV !== 'production';
const app = next({dev, dir: path.join(__dirname, '..', 'client')});
const handle = app.getRequestHandler();

app.prepare()
    .then(() => {
        const server = express();
        server.use('/assets', express.static(path.join(__dirname, '../public')));
        server.use('/graphql', (req, res, next) => {
            return expressGraphQL({
                schema: require('./schema'),
                graphiql: true,
            })(req, res, next)
        });

        server.get('/user', (req, res) => {
            res.redirect('/users');
        });

        server.get('/post', (req, res) => {
            res.redirect('/posts');
        });

        server.get('/posts/:id', (req, res) => {
            const actualPage = '/post';
            const queryParams = {id: req.params.id};
            app.render(req, res, actualPage, queryParams)
        });

        server.get('/users/:id', (req, res) => {
            const actualPage = '/user';
            const queryParams = {id: req.params.id}
            app.render(req, res, actualPage, queryParams)
        });


        server.get('*', (req, res) => {
            return handle(req, res)
        });

        server.listen(3000, (err) => {
            if (err) throw err;
            console.log('> Ready on http://localhost:3000')
        })
    })
    .catch((error) => {
        console.error(error.stack);
        process.exit(1)
    });